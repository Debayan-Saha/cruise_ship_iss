import pygame

pygame.init()

sc_obj = pygame.font.Font(None, 36)

speed = 0.2
# screen
screen = pygame.display.set_mode((800, 600))


class Player:
    def __init__(self, location, x, y):
        self.playerx = x
        self.playery = y
        self.ini_playerx = x
        self.ini_playery = y
        self.playerx_change = 0
        self.playery_change = 0
        self.level = 0
        self.life = 0
        self.totalscore = 0
        self.score = 0
        self.playerimg = pygame.image.load(location)
        self.time = 0
        self.tres = 0
        self.t = 0

    def timeup(self):
        return self.time

    def timechng(self):
        self.time += self.t - self.tres

    def tick(self):
        self.t = pygame.time.get_ticks()

    def tickres(self):
        self.tres = pygame.time.get_ticks()

    def print(self, x, y):
        screen.blit(self.playerimg, (x, y))

    def update(self):
        self.playerx += self.playerx_change
        self.playery += self.playery_change
        if self.playerx < 0:
            self.playerx = 0
        if self.playerx > 768:
            self.playerx = 768
        if self.playery < 0:
            self.playery = 0
        if self.playery > 568:
            self.playery = 568

    def lifechng(self, x):
        self.life = x

    def lifeup(self):
        return self.life

    def scorechng(self, x):
        self.score = x

    def totalchng(self):
        self.totalscore += self.score

    def totalup(self):
        return self.totalscore

    def scoreup(self):
        return self.score

    def setx(self, p):
        self.playerx_change = p

    def sety(self, q):
        self.playery_change = q

    def xchange(self):
        return self.playerx

    def ychange(self):
        return self.playery

    def levchange(self):
        self.level += 1

    def levelupdate(self):
        return self.level

    def restart(self):
        self.playerx = self.ini_playerx
        self.playery = self.ini_playery
        self.playerx_change = 0
        self.playery_change = 0


class Enemy:
    def __init__(self, location, x, y):
        self.enemyx = x
        self.enemyy = y
        self.enemyx_change = 0
        self.speedup = 0.25
        self.enemyimg = pygame.image.load(location)

    def print(self, x, y):
        screen.blit(self.enemyimg, (x, y))

    def update(self, lvl):
        self.enemyx += 0.25 + (lvl * self.speedup)
        if self.enemyx > 768:
            self.enemyx = 0

    def xchange(self):
        return self.enemyx

    def ychange(self):
        return self.enemyy

    def dist(self, x, y):
        self.dis = (((self.enemyx - x)) ** 2 + ((self.enemyy - y) ** 2)) ** 0.5
        if self.dis < 40:
            return True
        else:
            return False


class Stone:
    def __init__(self, location, x, y):
        self.stonex = x
        self.stoney = y
        self.stoneimg = pygame.image.load(location)

    def print(self, x, y):
        screen.blit(self.stoneimg, (x, y))

    def xchange(self):
        return self.stonex

    def ychange(self):
        return self.stoney

    def dist(self, x, y):
        self.dis = (((self.stonex - x)) ** 2 + ((self.stoney - y) ** 2)) ** 0.5
        if self.dis < 30:
            return True
        else:
            return False
