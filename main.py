import pygame
from config import *
import random

pygame.init()

# title
pygame.display.set_caption("Cruise Ships")

player1 = Player('./ship.png', 370, 568)
player2 = Player('./ship2.png', 370, 0)

enemy1 = Enemy('./battleship.png', -800, 46)
enemy2 = Enemy('./battleship.png', -400, 159)
enemy3 = Enemy('./battleship.png', -600, 272)
enemy4 = Enemy('./battleship.png', -200, 385)
enemy5 = Enemy('./battleship.png', -0, 498)

stone1 = Stone('stone.png', random.randint(1, 300), 0)
stone2 = Stone('stone.png', random.randint(500, 750), 0)

stone3 = Stone('stone.png', random.randint(1, 400), 113)
stone4 = Stone('stone.png', random.randint(401, 750), 113)

stone5 = Stone('stone.png', random.randint(1, 400), 226)
stone6 = Stone('stone.png', random.randint(401, 750), 226)

stone7 = Stone('stone.png', random.randint(1, 400), 339)
stone8 = Stone('stone.png', random.randint(401, 750), 339)

stone9 = Stone('stone.png', random.randint(1, 400), 452)
stone10 = Stone('stone.png', random.randint(401, 750), 452)

stone11 = Stone('stone.png', random.randint(1, 300), 565)
stone12 = Stone('stone.png', random.randint(451, 750), 565)

p = 1

EXIT = sc_obj.render("PRESS 'k' to EXIT", 1, (0, 0, 0))
player1.tickres()
# gameloop
running = True
while running:

    screen.fill((30, 144, 255))

    pygame.draw.rect(screen, [139, 69, 19], [0, 0, 800, 35])
    pygame.draw.rect(screen, [139, 69, 19], [0, 113, 800, 35])
    pygame.draw.rect(screen, [139, 69, 19], [0, 226, 800, 35])
    pygame.draw.rect(screen, [139, 69, 19], [0, 339, 800, 35])
    pygame.draw.rect(screen, [139, 69, 19], [0, 452, 800, 35])
    pygame.draw.rect(screen, [139, 69, 19], [0, 565, 800, 35])

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

        if event.type == pygame.KEYDOWN:
            if event.key == ord('k'):
                running = False

        if p == 1 and player1.lifeup() == 0:
            if event.type == pygame.KEYDOWN:

                if event.key == pygame.K_LEFT:
                    player1.setx(-speed)
                if event.key == pygame.K_RIGHT:
                    player1.setx(speed)
                if event.key == pygame.K_DOWN:
                    player1.sety(speed)
                if event.key == pygame.K_UP:
                    player1.sety(-speed)

            if event.type == pygame.KEYUP:
                if event.key == pygame.K_RIGHT:
                    player1.setx(0)
                if event.key == pygame.K_LEFT:
                    player1.setx(0)
                if event.key == pygame.K_DOWN:
                    player1.sety(0)
                if event.key == pygame.K_UP:
                    player1.sety(0)

        if p == 2 and player2.lifeup() == 0:
            if event.type == pygame.KEYDOWN:

                if event.key == ord('a'):
                    player2.setx(-speed)
                if event.key == ord('d'):
                    player2.setx(speed)
                if event.key == ord('s'):
                    player2.sety(speed)
                if event.key == ord('w'):
                    player2.sety(-speed)

            if event.type == pygame.KEYUP:
                if event.key == ord('d'):
                    player2.setx(0)
                if event.key == ord('a'):
                    player2.setx(0)
                if event.key == ord('s'):
                    player2.sety(0)
                if event.key == ord('w'):
                    player2.sety(0)

    if p == 1 and player1.lifeup() == 0:
        player1.update()
        player1.print(player1.xchange(), player1.ychange())

        enemy1.update(player1.levelupdate())
        enemy1.print(enemy1.xchange(), enemy1.ychange())

        enemy2.update(player1.levelupdate())
        enemy2.print(enemy2.xchange(), enemy2.ychange())
        enemy3.update(player1.levelupdate())
        enemy3.print(enemy3.xchange(), enemy3.ychange())
        enemy4.update(player1.levelupdate())
        enemy4.print(enemy4.xchange(), enemy4.ychange())
        enemy5.update(player1.levelupdate())
        enemy5.print(enemy5.xchange(), enemy5.ychange())

        stone1.print(stone1.xchange(), stone1.ychange())
        stone2.print(stone2.xchange(), stone2.ychange())

        stone3.print(stone3.xchange(), stone3.ychange())
        stone4.print(stone4.xchange(), stone4.ychange())

        stone5.print(stone5.xchange(), stone5.ychange())
        stone6.print(stone6.xchange(), stone6.ychange())

        stone7.print(stone7.xchange(), stone7.ychange())
        stone8.print(stone8.xchange(), stone8.ychange())

        stone9.print(stone9.xchange(), stone9.ychange())
        stone10.print(stone10.xchange(), stone10.ychange())

        stone11.print(stone11.xchange(), stone11.ychange())
        stone12.print(stone12.xchange(), stone12.ychange())

        if enemy1.dist(player1.xchange(), player1.ychange()):
            print("player1 lost")
            player1.tick()
            player1.restart()
            player1.timechng()
            player2.tickres()
            p = 2
            player1.totalchng()
            player1.lifechng(1)

        if enemy2.dist(player1.xchange(), player1.ychange()):
            print("player1 lost")
            player1.tick()
            player1.restart()
            player1.timechng()
            p = 2
            player2.tickres()
            player1.totalchng()
            player1.lifechng(1)

        if enemy3.dist(player1.xchange(), player1.ychange()):
            print("player1 lost")
            player1.tick()
            player1.restart()
            player1.timechng()
            p = 2
            player2.tickres()
            player1.totalchng()
            player1.lifechng(1)

        if enemy4.dist(player1.xchange(), player1.ychange()):
            print("player1 lost")
            player1.tick()
            player1.restart()
            player1.timechng()
            p = 2
            player2.tickres()
            player1.totalchng()
            player1.lifechng(1)

        if enemy5.dist(player1.xchange(), player1.ychange()):
            print("player1 lost")
            player1.tick()
            player1.restart()
            player1.timechng()
            p = 2
            player2.tickres()
            player1.totalchng()
            player1.lifechng(1)

        if stone1.dist(player1.xchange(), player1.ychange()):
            print("player1 lost")
            player1.tick()
            player1.restart()
            player1.timechng()
            player2.tickres()
            p = 2
            player1.totalchng()
            player1.lifechng(1)

        if stone2.dist(player1.xchange(), player1.ychange()):
            print("player1 lost")
            player1.tick()
            player1.restart()
            player1.timechng()
            player2.tickres()
            p = 2
            player1.totalchng()
            player1.lifechng(1)

        if stone3.dist(player1.xchange(), player1.ychange()):
            print("player1 lost")
            player1.tick()
            player1.restart()
            player1.timechng()
            player2.tickres()
            p = 2
            player1.totalchng()
            player1.lifechng(1)

        if stone4.dist(player1.xchange(), player1.ychange()):
            print("player1 lost")
            player1.tick()
            player1.restart()
            player1.timechng()
            player2.tickres()
            p = 2
            player1.totalchng()
            player1.lifechng(1)

        if stone5.dist(player1.xchange(), player1.ychange()):
            print("player1 lost")
            player1.tick()
            player1.restart()
            player1.timechng()
            player2.tickres()
            p = 2
            player1.totalchng()
            player1.lifechng(1)

        if stone6.dist(player1.xchange(), player1.ychange()):
            print("player1 lost")
            player1.tick()
            player1.restart()
            player1.timechng()
            player2.tickres()
            p = 2
            player1.totalchng()
            player1.lifechng(1)

        if stone7.dist(player1.xchange(), player1.ychange()):
            print("player1 lost")
            player1.tick()
            player1.restart()
            player1.timechng()
            player2.tickres()
            p = 2
            player1.totalchng()
            player1.lifechng(1)

        if stone8.dist(player1.xchange(), player1.ychange()):
            print("player1 lost")
            player1.tick()
            player1.restart()
            player1.timechng()
            player2.tickres()
            p = 2
            player1.totalchng()
            player1.lifechng(1)

        if stone9.dist(player1.xchange(), player1.ychange()):
            print("player1 lost")
            player1.tick()
            player1.restart()
            player1.timechng()
            player2.tickres()
            p = 2
            player1.totalchng()
            player1.lifechng(1)

        if stone10.dist(player1.xchange(), player1.ychange()):
            print("player1 lost")
            player1.tick()
            player1.restart()
            player1.timechng()
            player2.tickres()
            p = 2
            player1.totalchng()
            player1.lifechng(1)

        if stone11.dist(player1.xchange(), player1.ychange()):
            print("player1 lost")
            player1.tick()
            player1.restart()
            player1.timechng()
            player2.tickres()
            p = 2
            player1.totalchng()
            player1.lifechng(1)

        if stone12.dist(player1.xchange(), player1.ychange()):
            print("player1 lost")
            player1.tick()
            player1.restart()
            player1.timechng()
            player2.tickres()
            p = 2
            player1.totalchng()
            player1.lifechng(1)

        if 565 < player1.ychange() < 600:
            player1.scorechng(5)

        if 565 > player1.ychange() > 487:
            player1.scorechng(15)

        if 487 > player1.ychange() > 452:
            player1.scorechng(20)

        if 452 > player1.ychange() > 374:
            player1.scorechng(30)

        if 374 > player1.ychange() > 339:
            player1.scorechng(35)

        if 339 > player1.ychange() > 261:
            player1.scorechng(45)

        if 261 > player1.ychange() > 226:
            player1.scorechng(50)

        if 226 > player1.ychange() > 148:
            player1.scorechng(60)

        if 148 > player1.ychange() > 113:
            player1.scorechng(65)

        if 113 > player1.ychange() > 35:
            player1.scorechng(75)

        if player1.ychange() < 35:
            player1.scorechng(80)

        if player1.ychange() < 10:
            player1.tick()
            player1.restart()
            player1.timechng()
            # print("ewifwj" ,player1.timeup())
            player1.levchange()
            player1.lifechng(0)
            player1.totalchng()
            # print("LLSLLSLKlayer1  ",player1.totalup())
            if player2.lifeup() == 0:
                p = 2
                player2.tickres()
            else:
                p = 1
                player1.tickres()

    if p == 2 and player2.lifeup() == 0:
        player2.update()
        player2.print(player2.xchange(), player2.ychange())
        enemy1.update(player2.levelupdate())
        enemy1.print(enemy1.xchange(), enemy1.ychange())
        enemy2.update(player2.levelupdate())
        enemy2.print(enemy2.xchange(), enemy2.ychange())
        enemy3.update(player2.levelupdate())
        enemy3.print(enemy3.xchange(), enemy3.ychange())
        enemy4.update(player2.levelupdate())
        enemy4.print(enemy4.xchange(), enemy4.ychange())
        enemy5.update(player2.levelupdate())
        enemy5.print(enemy5.xchange(), enemy5.ychange())

        stone1.print(stone1.xchange(), stone1.ychange())
        stone2.print(stone2.xchange(), stone2.ychange())

        stone3.print(stone3.xchange(), stone3.ychange())
        stone4.print(stone4.xchange(), stone4.ychange())

        stone5.print(stone5.xchange(), stone5.ychange())
        stone6.print(stone6.xchange(), stone6.ychange())

        stone7.print(stone7.xchange(), stone7.ychange())
        stone8.print(stone8.xchange(), stone8.ychange())

        stone9.print(stone9.xchange(), stone9.ychange())
        stone10.print(stone10.xchange(), stone10.ychange())

        stone11.print(stone11.xchange(), stone11.ychange())
        stone12.print(stone12.xchange(), stone12.ychange())

        if enemy1.dist(player2.xchange(), player2.ychange()):
            print("player2 lost")
            player2.tick()
            player2.restart()
            player2.timechng()
            player1.tickres()
            p = 1
            player2.totalchng()
            player2.lifechng(1)

        if enemy2.dist(player2.xchange(), player2.ychange()):
            print("player2 lost")
            player2.tick()
            player2.restart()
            player2.timechng()
            p = 1
            player1.tickres()
            player2.totalchng()
            player2.lifechng(1)

        if enemy3.dist(player2.xchange(), player2.ychange()):
            print("player2 lost")
            player2.tick()
            player2.restart()
            player2.timechng()
            p = 1
            player1.tickres()
            player2.totalchng()
            player2.lifechng(1)

        if enemy4.dist(player2.xchange(), player2.ychange()):
            print("player2 lost")
            player2.tick()
            player2.restart()
            player2.timechng()
            p = 1
            player1.tickres()
            player2.totalchng()
            player2.lifechng(1)

        if enemy5.dist(player2.xchange(), player2.ychange()):
            print("player2 lost")
            player2.tick()
            player2.restart()
            player2.timechng()
            p = 1
            player1.tickres()
            player2.totalchng()
            player2.lifechng(1)

        if stone1.dist(player2.xchange(), player2.ychange()):
            print("player2 lost")
            player2.tick()
            player2.restart()
            player2.timechng()
            player1.tickres()
            p = 1
            player2.totalchng()
            player2.lifechng(1)

        if stone2.dist(player2.xchange(), player2.ychange()):
            print("player2 lost")
            player2.tick()
            player2.restart()
            player2.timechng()
            player1.tickres()
            p = 1
            player2.totalchng()
            player2.lifechng(1)

        if stone3.dist(player2.xchange(), player2.ychange()):
            print("player2 lost")
            player2.tick()
            player2.restart()
            player2.timechng()
            player1.tickres()
            p = 1
            player2.totalchng()
            player2.lifechng(1)

        if stone4.dist(player2.xchange(), player2.ychange()):
            print("player2 lost")
            player2.tick()
            player2.restart()
            player2.timechng()
            player1.tickres()
            p = 1
            player2.totalchng()
            player2.lifechng(1)

        if stone5.dist(player2.xchange(), player2.ychange()):
            print("player2 lost")
            player2.tick()
            player2.restart()
            player2.timechng()
            player1.tickres()
            p = 1
            player2.totalchng()
            player2.lifechng(1)

        if stone6.dist(player2.xchange(), player2.ychange()):
            print("player2 lost")
            player2.tick()
            player2.restart()
            player2.timechng()
            player1.tickres()
            p = 1
            player2.totalchng()
            player2.lifechng(1)

        if stone7.dist(player2.xchange(), player2.ychange()):
            print("player2 lost")
            player2.tick()
            player2.restart()
            player2.timechng()
            player1.tickres()
            p = 1
            player2.totalchng()
            player2.lifechng(1)

        if stone8.dist(player2.xchange(), player2.ychange()):
            print("player2 lost")
            player2.tick()
            player2.restart()
            player2.timechng()
            player1.tickres()
            p = 1
            player2.totalchng()
            player2.lifechng(1)

        if stone9.dist(player2.xchange(), player2.ychange()):
            print("player2 lost")
            player2.tick()
            player2.restart()
            player2.timechng()
            player1.tickres()
            p = 1
            player2.totalchng()
            player2.lifechng(1)

        if stone10.dist(player2.xchange(), player2.ychange()):
            print("player2 lost")
            player2.tick()
            player2.restart()
            player2.timechng()
            player1.tickres()
            p = 1
            player2.totalchng()
            player2.lifechng(1)

        if stone11.dist(player2.xchange(), player2.ychange()):
            print("player2 lost")
            player2.tick()
            player2.restart()
            player2.timechng()
            player1.tickres()
            p = 1
            player2.totalchng()
            player2.lifechng(1)

        if stone12.dist(player2.xchange(), player2.ychange()):
            print("player2 lost")
            player2.tick()
            player2.restart()
            player2.timechng()
            player1.tickres()
            p = 1
            player2.totalchng()
            player2.lifechng(1)

        if 45 > player2.ychange() > 0:
            player2.scorechng(5)

        if 113 > player2.ychange() > 45:
            player2.scorechng(15)

        if 113 > player2.ychange() > 35:
            player2.scorechng(15)

        if 148 > player2.ychange() > 113:
            player2.scorechng(20)

        if 226 > player2.ychange() > 148:
            player2.scorechng(30)

        if 261 > player2.ychange() > 226:
            player2.scorechng(35)

        if 339 > player2.ychange() > 261:
            player2.scorechng(45)

        if 374 > player2.ychange() > 339:
            player2.scorechng(50)

        if 452 > player2.ychange() > 374:
            player2.scorechng(60)

        if 487 > player2.ychange() > 452:
            player2.scorechng(65)

        if 555 > player2.ychange() > 487:
            player2.scorechng(75)

        if player2.ychange() > 555:
            player2.scorechng(80)

        if player2.ychange() > 560:
            player2.tick()
            player2.restart()
            player2.timechng()
            # print("ewifwj" ,player2.timeup())
            player2.levchange()
            player2.lifechng(0)
            player2.totalchng()
            # print("LLSLLSLKlayer2  ",player2.totalup())
            if player1.lifeup() == 0:
                p = 1
                player1.tickres()
            else:
                p = 2
                player2.tickres()

    sc1 = sc_obj.render("SCORE 1: " + str(player1.totalup()), 1, (0, 0, 0))
    screen.blit(sc1, (0, 0))

    sc2 = sc_obj.render("SCORE 2: " + str(player2.totalup()), 1, (0, 0, 0))
    screen.blit(sc2, (635, 0))

    start = sc_obj.render("START", 1, (0, 0, 0))
    end = sc_obj.render("END", 1, (0, 0, 0))
    if p == 1:
        screen.blit(start, (420, 575))
        screen.blit(end, (420, 5))

    if p == 2:
        screen.blit(start, (420, 5))
        screen.blit(end, (420, 575))

    if player1.lifeup() == 1 and player2.lifeup() == 1:
        if player1.totalup() == player2.totalup():
            if player1.timeup() > player2.timeup():
                winner11 = sc_obj.render("WINNER PLAYER 1  " + str(player1.timeup() / 1000) + " sec", 1, (0, 0, 0))
                screen.blit(winner11, (250, 200))
                screen.blit(EXIT, (300, 390))
            else:
                winner22 = sc_obj.render("WINNER PLAYER 2  " + str(player2.timeup() / 1000) + " sec", 1, (0, 0, 0))
                screen.blit(winner22, (250, 200))
                screen.blit(EXIT, (300, 390))

        if player1.totalup() > player2.totalup():
            winner1 = sc_obj.render("WINNER PLAYER 1  " + str(player1.timeup() / 1000) + " sec", 1, (0, 0, 0))
            screen.blit(winner1, (250, 200))
            screen.blit(EXIT, (300, 390))

        if player1.totalup() < player2.totalup():
            winner2 = sc_obj.render("WINNER PLAYER 2  " + str(player2.timeup() / 1000) + " sec", 1, (0, 0, 0))
            screen.blit(winner2, (250, 200))
            screen.blit(EXIT, (300, 390))

    pygame.display.update()
